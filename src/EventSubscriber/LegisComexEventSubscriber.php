<?php

  /**
  * @file
  * Contains \Drupal\legiscomex_asyncdata\EventSubscriber\LegisComexEventSubscriber.
  */

  namespace Drupal\legiscomex_asyncdata\EventSubscriber;

  use Symfony\Component\HttpKernel\KernelEvents;
  use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;
  use Drupal\legiscomex_asyncdata\Controller\XmlController;

  /**
  * Event Subscriber MyEventSubscriber.
  */
  class LegisComexEventSubscriber implements EventSubscriberInterface {

    /**
    * Code that should be triggered on event specified
    */
    public function onRespond(FilterResponseEvent $event) {
      // The RESPONSE event occurs once a response was created for replying to a request.
      // For example you could override or add extra HTTP headers in here
      $response = $event->getResponse();
      $response->headers->set('X-Custom-Header', 'MyValue');

      \Drupal::service('page_cache_kill_switch')->trigger();

      $user = \Drupal::currentUser();
      //Si el usuario es administrador, no capturar datos
      if(!in_array("administrator", $user->getRoles())) {
        //Instanciacion de XmlController. web/modules/custom/legiscomex_asyncdata/src/Controller/XmlController.php
        $xml = new XmlController;

        //Verificacion de usuario. Logueado\Anonimo
        if ($user->id() || $user->id() != 0) {
          $user_id = $user->id();
        }else {
          $user_id = 'anonymous';
        }

        //Definicion de variables - Datos
        $date = date('Ymd_Gis');//isset($params['date']) ? Xss::filter($params['date']) : NULL;
        $current_path = \Drupal::service('path.current')->getPath();
        $nid = NULL;
        $node = \Drupal::routeMatch()->getParameter('node');
        if ($node instanceof \Drupal\node\NodeInterface) {
          $nid = $node->Id();
        }
        $ip_client = \Drupal::request()->getClientIp();
        $http_refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL;
        $x_forwarded_for = isset($_SERVER['X_FORWARDED_FOR']) ? $_SERVER['X_FORWARDED_FOR'] : NULL;
        $language = $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $country = 'CO';
        $application = '33';

        //Preparacion de datos a ser enviados
        $params = [
          'user' => $user_id,
          'date' => $date,
          'ip_client' => $ip_client,
          'http_refer' => $http_refer,
          'X_FORWARDED_FOR' => $x_forwarded_for,
          'nid' => $nid,
          'current_path' => $current_path,
          'language' => $language,
          'country' => $country,
          'application' => $application
        ];

        //Llamado a la funcion receiveData con @params
        $xml->receiveData($params);
      }

    }

    /**
    * {@inheritdoc}
    */
    public static function getSubscribedEvents() {
      // For this example I am using KernelEvents constants (see below a full list).
      $events[KernelEvents::RESPONSE][] = ['onRespond'];
      return $events;
    }

  }
