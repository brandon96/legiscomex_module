<?php

namespace Drupal\legiscomex_asyncdata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigRouteForm.
 */
class ConfigRouteForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'legiscomex_asyncdata.configroute',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_route_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('legiscomex_asyncdata.configroute');

    $form['#tree'] = TRUE;

    $form['bootstrap'] = [
      '#type' => 'vertical_tabs',
      '#weight' => -10,
      '#default_tab' => 'route'
    ];

    $route = $config->get('route');
    $form['route'] = [
      '#type' => 'details',
      '#title' => t('Ruta de Archivos'),
      '#group' => 'bootstrap'
    ];

    $form['route']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ruta'),
      '#default_value' => $route['path'],
      '#description' => 'Ruta del sistema que será usada para almacenar los registros de lectura de navegación LegisComex. Ej:"public://dir_name/".<br>Por defecto se almacenan en public://legiscomex_uso/.',
      '#required' => false,
    ];

    $nav = $config->get('nav');
    $form['nav'] = [
      '#type' => 'details',
      '#title' => t('Estado'),
      '#group' => 'bootstrap'
    ];

    $form['nav']['read'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activar'),
      '#default_value' => $nav['read'],
      '#description' => 'Si activa el módulo, se iniciará la captura de información de la navegación. Al desactivarlo se apaga la captura de información, pero la configuración permanece.'
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('legiscomex_asyncdata.configroute');
    $config->set('route', $form_state->getValue('route'));
    $config->set('nav', $form_state->getValue('nav'));
    $config->save();
  }

}
