<?php

namespace Drupal\legiscomex_asyncdata\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation;
//use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\file\Entity\File;

/**
 * Class XmlController.
 */
class XmlController extends ControllerBase {

  // recibe los parametros enviados en web/modules/custom/legiscomex_asyncdata/legiscomex_asyncdata.module
  /**
   * receiveData.
   *
   * @return json
   *   Return json.
   */
  public function receiveData($params = []) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $config = \Drupal::config('legiscomex_asyncdata.configroute');
    $nav = $config->get('nav');

    $params_url = \Drupal::request()->query->all();
    // valida si esta marcado el check de Activar lectura en el formulario de configuracion /admin/config/legiscomex_asyncdata/configroute
    // llamado por web/modules/custom/legiscomex_asyncdata/src/Form/ConfigRouteForm.php
    if ($nav['read']) {
      if (!empty($params)) {
        // si se enviaron parametros, prepara el array con [data] y llama al metodo saveFile para realizar el proceso de creacion de archivo
        $params_values = ['data' => $params];
        $this->saveFile($params_values);  
        return new JsonResponse([
          $params_values,
          'method' => 'GET'
        ]);
      }elseif($params_url) {
        return new JsonResponse([
          $this->getResults($params_url),
          'method' => 'GET'
        ]);        
      }else{
        \Drupal::logger('legiscomex_asyncdata')->notice("No se recibieron parámetros");        
        return new JsonResponse([
          'error' => 'no params'
        ]);             
      }
    }else {
      return new JsonResponse([
        'response' => 'Read disabled',
      ]);   
    }
  }

  //funcion que se ejecuta si es llamada desde GET en /legiscomex-asyncdata/store-nav con  query @params
  private function getResults($params = NULL){
    \Drupal::service('page_cache_kill_switch')->trigger();

    if ($params) {
      //Params filters - Xss
      $token = isset($params['token']) ? Xss::filter($params['token']) : NULL;
      $application = isset($params['application']) ? Xss::filter($params['application']) : 33;
      $action = isset($params['action']) ? Xss::filter($params['action']) : 0;
      $sector = isset($params['sector']) ? Xss::filter($params['sector']) : "";
      $company = isset($params['company']) ? Xss::filter($params['company']) : NULL;
      $user = isset($params['user']) ? Xss::filter($params['user']) : NULL;
      $date = date('Ymd_Gis');//isset($params['date']) ? Xss::filter($params['date']) : NULL;
      $language = isset($params['language']) ? Xss::filter($params['language']) : "es-CO";
      //$product = isset($params['product']) ? Xss::filter($params['product']) : "";
      $exchange = isset($params['exchange']) ? Xss::filter($params['exchange']) : "";
      $use = isset($params['use']) ? Xss::filter($params['use']) : NULL;
      $transaction = isset($params['transaction']) ? Xss::filter($params['transaction']) : NULL;
      $report = isset($params['report']) ? Xss::filter($params['report']) : "";
      $query_string = isset($params['query_string']) ? Xss::filter($params['query_string']) : NULL;
      $tool = isset($params['tool']) ? Xss::filter($params['tool']) : "SIC";
      $module = isset($params['module']) ? Xss::filter($params['module']) : NULL;
      $country = isset($params['country']) ? Xss::filter($params['country']) : "CO";

      //Array values from params
      $params_values = [
        'token' => $token,
        'application' => $application,
        'action' => $action,
        'sector' => $sector,
        'company' => $company,
        'user' => $user,
        'date' => $date,
        'language' => $language,
        #'product' => $product,
        'exchange' => $exchange,
        'use' => $use,
        'transaction' => $transaction,
        'report' => $report,
        'query_string' => $query_string,
        'tool' => $tool,
        'module' => $module,
        'country' => $country,
        'ip_client' => \Drupal::request()->getClientIp(),
        'http_refer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL,
        'X_FORWARDED_FOR' => isset($_SERVER['X_FORWARDED_FOR']) ? $_SERVER['X_FORWARDED_FOR'] : NULL,
        'guid' => '',
        'nid' => ''
      ];

      return [
        'data' => $params_values,
      ];      
    }else {
      \Drupal::logger('legiscomex_asyncdata')->notice("No se recibieron parámetros");
      return [
        'error' => 'No params',
      ];      
    }    
    
  }

  private function saveFile($values){
    //Config Route - Path
    $config = \Drupal::config('legiscomex_asyncdata.configroute');
    $route = $config->get('route');

    //Create directory from path
    $path = !empty($route['path']) ? $route['path'] : 'public://legiscomex_uso/';
    $directory = $path;
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);

    // //Nomenclature
    // ej: 33_20190426_201958_127_0_0_1_4323f59b-c8b2-46bf-b3d5-22d580f35201.txt
    // 33_yyyyMMdd_hhmmss_AdressIP_GUID.txt

    // Filename and uri
    $filename = "33_".$values['data']['date'].$values['data']['ip_client'].".txt";
    $uri = $directory.$filename;

    //XML strcuture
    $content = "<root><estructura nombre='Uso'><campo nombre='APLICACION' valor='".$values['data']['application']."' />";
    $content .= "<campo nombre='IP_CLIENT' valor='".$values['data']['ip_client']."' /><campo nombre='HTTP_REFER' valor='".$values['data']['http_refer']."' /><campo nombre='X_FORWARDED_FOR' valor='".$values['data']['X_FORWARDED_FOR']."' />";
    $content .= "<campo nombre='current_path' valor='".$values['data']['current_path']."' /><campo nombre='IDIOMA' valor='".$values['data']['language']."' />";
    $content .= "<campo nombre='USUARIO' valor='".$values['data']['user']."' /><campo nombre='FECHA' valor='".$values['data']['date']."' /><campo nombre='Nodo' valor='".$values['data']['nid']."' />";
    $content .= "<campo nombre='PAIS' valor='".$values['data']['country']."' />";
    $content .= "</estructura></root>";

    
    //File create
    try {
      if (!file_exists($uri)) {
        file_save_data($content, $uri, FILE_EXISTS_REPLACE);
        \Drupal::logger('legiscomex_asyncdata')->notice("Archivo creado. ".$uri);
      }      
    } catch (\Throwable $th) {
      \Drupal::logger('legiscomex_asyncdata')->error("Archivo ".$uri." no se pudo crear. Fecha: ".$$values['data']['date']." IP: ".$values['data']['ip_client']);
    }


  }

}
